FROM maven:3.6.0-jdk-8-alpine
MAINTAINER Nobuyuki Aoki <aoki@sparqlite.com>

# Copy scripts - mounted onto local folder
COPY . /workspace/tcarprdf
# Copy source for building jar file

COPY . /tcarprdfbuild
RUN mkdir /root/.repository
COPY config /root/.repository/config

# no need to build scripts
# RUN cd workspace/tcarprdf;mvn -U -Dmaven.repo.local=/.m2/repository clean compile assembly:single

RUN cd tcarprdfbuild;mvn -U -Dmaven.repo.local=/.m2/repository clean compile assembly:single

RUN apk upgrade --update && \
    apk add lftp && \
    rm -rf /var/cache/apk/* && rm -rf /tmp/*

COPY run.sh /run.sh
# docker-compose up should be run first to mount/copy workspace folder with source code.
# CMD ["/workspace/tcarprdf/bin/LocalGlyTouCanID > /workspace/tcarprdf/bin/glytoucan.tsv"]
CMD ["/run.sh"]
