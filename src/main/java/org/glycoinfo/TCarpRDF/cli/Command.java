package org.glycoinfo.TCarpRDF.cli;

public interface Command {
  String name();

  String description();

  void run(String[] args);
}
