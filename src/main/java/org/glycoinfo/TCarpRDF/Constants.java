package org.glycoinfo.TCarpRDF;

public class Constants {
  public static final String CONFIG_DIRECTORY_NAME = ".repository";
  public static final String CONFIG_FILE_NAME = "config";

  //public static final String PDB_HOST = "ftp.pdbj.org";
  //https://data.pdbj.org/pdbjplus/data/cc/mmcif
  public static final String PDB_HOST = "data.pdbj.org";

  //public static final String PDB_ALL_PDBID_URL = "ftp://ftp.pdbj.org/XML/pdbmlplus/all_pdbid.txt.gz";
  public static final String PDB_ALL_PDBID_URL = "https://pdbj.org/rest/newweb/search/sql?q=select+pdbid+from+pdbj.brief_summary+order+by+pdbid+asc&format=csv&header=0";
  //public static final String PDB_ALL_PDBID_URL = "https://gitlab.com/glyconavi/resources/-/raw/master/PDB/all_pdbid.txt";
  //public static final String PDB_ALL_PDBID_URL = "https://gitlab.com/glyconavi/resources/-/raw/master/PDB/all_pdbid.txt.gz";
  //public static final String PDB_ALL_PDBID_URL = "ftp://data.pdbj.org/pub/pdb/holdings/current_file_holdings.json.gz";
  
  // 2021 version
  // public static final String PDB_ALL_PDBID_URL = "ftp://data.pdbjbk1.pdbj.org/pub/pdb/holdings/released_structures_last_modified_dates.json.gz";
  
  //public static final String PDB_LATEST_NEW_PDBID_URL = "ftp://ftp.pdbj.org/XML/pdbmlplus/latest_new_pdbid.txt";
  //public static final String PDB_LATEST_NEW_PDBID_URL = "https://pdbj.org/rest/newweb/search/sql?q=select+pdbid+from+pdbj.brief_summary+where+release_date%3DUPDATE_DATE%28%29+order+by+pdbid+asc&format=csv&header=0";
  public static final String PDB_LATEST_NEW_PDBID_URL = "ftp://data.pdbj.org/pub/pdb/data/status/latest/added.pdb";

  //public static final String PDB_LATEST_UPDATED_PDBID_URL = "ftp://ftp.pdbj.org/XML/pdbmlplus/latest_updated_pdbid.txt";
  //public static final String PDB_LATEST_UPDATED_PDBID_URL = "https://pdbj.org/rest/newweb/search/sql?q=select+pdbid+from+pdbj.brief_summary+where+modification_date%3DUPDATE_DATE%28%29+and+release_date%21%3DUPDATE_DATE%28%29+order+by+pdbid+asc&format=csv&header=0";
  public static final String PDB_LATEST_UPDATED_PDBID_URL = "ftp://data.pdbj.org/pub/pdb/data/status/latest/modified.pdb";

  //public static final String PDB_ALL_OBSOLETE_PDBID_URL = "ftp://ftp.pdbj.org/XML/pdbmlplus/all_obsolete_pdbid.txt";
  //public static final String PDB_ALL_OBSOLETE_PDBID_URL = "https://pdbj.org/rest/newweb/search/sql?q=select+pdbid+from+misc.pdb_obsolete+order+by+obsolete_date+desc&format=csv&header=0";
  public static final String PDB_ALL_OBSOLETE_PDBID_URL = "ftp://data.pdbj.org/pub/pdb/data/status/latest/obsolete.pdb";

  //public static final String PDB_MMCIF_ALL_PATH = "/mmcif";
  public static final String PDB_MMCIF_ALL_PATH = "/pub/pdb/data/structures/all/mmCIF";
  // https://data.pdbj.org/pub/pdb/data/structures/all/mmCIF/

  //public static final String PDB_MMCIF_CC_PATH = "/mine2/data/cc/mmcif";
  //public static final String PDB_MMCIF_CC_PATH = "pdbjplus/data/cc/mmcif";
  public static final String PDB_MMCIF_CC_PATH = "/pdbjplus/data/cc/mmcif";
  // https://ftp.pdbj.org/pdbjplus/data/cc/mmcif/

  // https://data.pdbj.org/pdbjplus/data/cc/mmcif/

  //public static final String PDB_MMJSON_ALL_PATH = "/mine2/data/mmjson/all";
  public static final String PDB_MMJSON_ALL_PATH = "/pdbjplus/data/pdb/mmjson";
  // https://data.pdbj.org/pdbjplus/data/pdb/mmjson/

  //public static final String PDB_MMJSON_CC_PATH = "/mine2/data/cc/mmjson";
  public static final String PDB_MMJSON_CC_PATH = "/pdbjplus/data/cc/mmjson";
  // https://data.pdbj.org/pdbjplus/data/cc/mmjson/

}
