package org.glycoinfo.TCarpRDF.repository;

import static org.glycoinfo.TCarpRDF.Constants.*;
import static org.glycoinfo.TCarpRDF.cli.App.*;
import static org.glycoinfo.TCarpRDF.repository.Utils.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.ZonedDateTime;
import java.util.zip.GZIPInputStream;

import org.glycoinfo.PDB2Glycan.cli.Converter;
import org.glycoinfo.PDB2Glycan.cli.InputFormat;
import org.glycoinfo.PDB2Glycan.cli.OutputFormat;
import org.glycoinfo.TCarpRDF.config.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import com.fasterxml.jackson.databind.JsonNode;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import org.json.JSONObject;
//import org.json.JSONException;
//import org.json.JSONArray;

//import org.json.simple.JSONArray;
//import org.json.simple.JSONObject;
//import org.json.simple.parser.*;

public class Repository {

  private static final Logger logger = LoggerFactory.getLogger(Repository.class);

  enum InputType {
    MMCIF("mmcif"),
    MMJSON("mmjson");

    private final String baseName;

    InputType(String baseName) {
      this.baseName = baseName;
    }

    public String getBaseName() {
      return baseName;
    }
  }

  enum OutputType {
    JSON("json"),
    JSONLD("jsonld"),
    TURTLE("turtle");

    private final String baseName;

    OutputType(String baseName) {
      this.baseName = baseName;
    }

    public String getBaseName() {
      return baseName;
    }
  }

  enum PDBRelease {
/*
    ALL_PDB_ID("all_pdbid.txt.gz"),
    LATEST_NEW_PDB_ID("latest_new_pdbid.txt"),
    LATEST_UPDATE_PDB_ID("latest_updated_pdbid.txt"),
    ALL_OBSOLETE_PDB_ID("all_obsolete_pdbid.txt");
*/
    //ALL_PDB_ID("all_pdbid.txt.gz"),
    
    //ALL_PDB_ID("current_file_holdings.json.gz"),
    ALL_PDB_ID("sql_query.csv"),
    // 2021 version
    // ALL_PDB_ID("released_structures_last_modified_dates.json.gz"),
    LATEST_NEW_PDB_ID("added.pdb"),
    LATEST_UPDATE_PDB_ID("modified.pdb"),
    ALL_OBSOLETE_PDB_ID("obsolete.pdb");


    private final String baseName;

    PDBRelease(String baseName) {
      this.baseName = baseName;
    }

    public String getBaseName() {
      return baseName;
    }
  }

  private Configuration config;

  public Repository() {
    this(new Configuration());
  }

  public Repository(Configuration configuration) {
    this.config = configuration;
  }

  public void update() {
    update(true, true, false);
  }

  public void update(boolean synchronize, boolean convert, boolean all) {
    if (synchronize) {
      prepareSyncDirectory();
      obtainReleases();
      synchronize();
    }

    if (!convert) {
      return;
    }

    if (all) {
      cleanOutDirectory();
      prepareOutDirectory();

      logger.info(String.format("Convert entries listed in %s", PDBRelease.ALL_PDB_ID));
      //convertGZipList(getReleasePath(PDBRelease.ALL_PDB_ID));
      
      // 2021 version
      //convertJSONGZIPList(getReleasePath(PDBRelease.ALL_PDB_ID));
      convertList(getReleasePath(PDBRelease.ALL_PDB_ID));
    } else {
      prepareOutDirectory();

      logger.info(String.format("Convert entries listed in %s", PDBRelease.LATEST_NEW_PDB_ID));
      convertList(getReleasePath(PDBRelease.LATEST_NEW_PDB_ID));

      logger.info(String.format("Convert entries listed in %s", PDBRelease.LATEST_UPDATE_PDB_ID));
      convertList(getReleasePath(PDBRelease.LATEST_UPDATE_PDB_ID));

      logger.info(String.format("Remove output entries listed in %s", PDBRelease.ALL_OBSOLETE_PDB_ID));
      removeList(getReleasePath(PDBRelease.ALL_OBSOLETE_PDB_ID));
    }
  }

  private Path getInputPath(InputType type) {
    return Paths.get(config.repository.getPath(), type.getBaseName());
  }

  private Path getReleasePath(PDBRelease type) {
    return Paths.get(config.repository.getPath(), type.getBaseName());
  }

  private Path getOutputPath(OutputType type) {
    return Paths.get(config.repository.getPath(), "out", type.getBaseName());
  }

  private void prepareSyncDirectory() {
    try {
      if (config.repository.isMmcif()) {
        mkdir(getInputPath(InputType.MMCIF));
      }
      if (config.repository.isMmjson()) {
        mkdir(getInputPath(InputType.MMJSON));
      }
    } catch (IOException e) {
      logger.error(e.getMessage(), e);
      System.exit(STATUS_UPDATE_ERROR);
    }
  }

  private void obtainReleases() {
    try {
      logger.info(String.format("Download %s", PDB_ALL_PDBID_URL));
      wget(PDB_ALL_PDBID_URL, getReleasePath(PDBRelease.ALL_PDB_ID).toAbsolutePath().toString());

      logger.info(String.format("Download %s", PDB_LATEST_NEW_PDBID_URL));
      wget(PDB_LATEST_NEW_PDBID_URL, getReleasePath(PDBRelease.LATEST_NEW_PDB_ID).toAbsolutePath().toString());

      logger.info(String.format("Download %s", PDB_LATEST_UPDATED_PDBID_URL));
      wget(PDB_LATEST_UPDATED_PDBID_URL, getReleasePath(PDBRelease.LATEST_UPDATE_PDB_ID).toAbsolutePath().toString());

      logger.info(String.format("Download %s", PDB_ALL_OBSOLETE_PDBID_URL));
      wget(PDB_ALL_OBSOLETE_PDBID_URL, getReleasePath(PDBRelease.ALL_OBSOLETE_PDB_ID).toAbsolutePath().toString());
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      System.exit(STATUS_UPDATE_ERROR);
    }
  }

  private void synchronize() {
    try {
      if (config.repository.isMmcif()) {
        logger.info(String.format("Synchronize files with %s%s", PDB_HOST, PDB_MMCIF_ALL_PATH));
        lftp(PDB_HOST, PDB_MMCIF_ALL_PATH, getInputPath(InputType.MMCIF).resolve("all").toAbsolutePath().toString());

        logger.info(String.format("Synchronize files with %s%s", PDB_HOST, PDB_MMCIF_CC_PATH));
        lftp(PDB_HOST, PDB_MMCIF_CC_PATH, getInputPath(InputType.MMCIF).resolve("cc").toAbsolutePath().toString());
      }
      if (config.repository.isMmjson()) {
        logger.info(String.format("Synchronize files with %s%s", PDB_HOST, PDB_MMJSON_ALL_PATH));
        lftp(PDB_HOST, PDB_MMJSON_ALL_PATH, getInputPath(InputType.MMJSON).resolve("all").toAbsolutePath().toString());

        logger.info(String.format("Synchronize files with %s%s", PDB_HOST, PDB_MMJSON_CC_PATH));
        lftp(PDB_HOST, PDB_MMJSON_CC_PATH, getInputPath(InputType.MMJSON).resolve("cc").toAbsolutePath().toString());
      }
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      System.exit(STATUS_UPDATE_ERROR);
    }
  }

  private void prepareOutDirectory() {
    try {
      if (config.converter.isJson()) {
        mkdir(getOutputPath(OutputType.JSON));
      }
      if (config.converter.isJsonld()) {
        mkdir(getOutputPath(OutputType.JSONLD));
      }
      if (config.converter.isTurtle()) {
        mkdir(getOutputPath(OutputType.TURTLE));
      }
    } catch (IOException e) {
      logger.error(e.getMessage(), e);
      System.exit(STATUS_UPDATE_ERROR);
    }
  }

  private void cleanOutDirectory() {
    try {
      rmdir(getOutputPath(OutputType.JSON));
      rmdir(getOutputPath(OutputType.JSONLD));
      rmdir(getOutputPath(OutputType.TURTLE));
    } catch (IOException e) {
      logger.error(e.getMessage(), e);
      System.exit(STATUS_UPDATE_ERROR);
    }
  }

  private void convertList(Path list) {
    try (BufferedReader reader = Files.newBufferedReader(list)) {
      reader.lines().forEach(this::convert);
    } catch (IOException e) {
      logger.error(e.getMessage(), e);
      System.exit(STATUS_UPDATE_ERROR);
    }
  }

  private void convertGZipList(Path list) {
    try (BufferedReader reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(list.toString()))))) {
      reader.lines().forEach(this::convert);
    } catch (IOException e) {
      logger.error(e.getMessage(), e);
      System.exit(STATUS_UPDATE_ERROR);
    }
  }
/*
  private void convertJSONGZIPList(Path list) {
    System.out.println(list.toString());
    try {
      Object ob = new JSONParser().parse(new FileReader(list.toString()));
      // typecasting ob to JSONObject
      JSONObject js = (JSONObject) ob;
      JSONArray items = js.getJSONArray("");
      for (int i = 0; i < items.length(); i++)
      {
        this.convert(items.get(i).toString());
      }
    } catch (IOException e) {
      logger.error(e.getMessage(), e);
      System.exit(STATUS_UPDATE_ERROR);
    }
  }
*/
  private void convert(String code) {
	  String input = config.converter.getInput();
	  Converter converter = new Converter()
        .noNetwork(true)
        .pdbCode(code)
        .force(true)
        .inputFormat(InputFormat.lookup(input));

    switch (input) {
      case "mmcif":
      case "mmjson":
        Path basePath = getInputPath(InputType.valueOf(input.toUpperCase()));
        converter.ccPath(basePath.resolve("cc").toAbsolutePath().toString());
        converter.pdbPath(basePath.resolve("all").toAbsolutePath().toString());
        break;
      default:
        throw new IllegalArgumentException("Unknown argument: " + input);
    }

    try {

      if (config.converter.isJson()) {
        logger.debug(String.format("Convert %s to JSON", code));

        converter.outputFilePath(getOutputPath(OutputType.JSON).toAbsolutePath().toString())
            .outputFormat(OutputFormat.JSON)
            .run();
      }
      if (config.converter.isJsonld()) {
        logger.debug(String.format("Convert %s to JSON-LD", code));

        converter.outputFilePath(getOutputPath(OutputType.JSONLD).toAbsolutePath().toString())
            .outputFormat(OutputFormat.JSONLD)
            .run();
      }
      if (config.converter.isTurtle()) {
        logger.debug(String.format("Convert %s to Turtle", code));

        converter.outputFilePath(getOutputPath(OutputType.TURTLE).toAbsolutePath().toString())
            .outputFormat(OutputFormat.RDF_TURTLE)
            .run();
      }

    } catch (Exception e) {

    	Path path = Paths.get("./ConvertErrorId.txt");
    	try {
                FileOutputStream fileOutputStream = new FileOutputStream(path.toFile(), true);
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream,  "UTF-8");
                BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);

    			ZonedDateTime zdt = ZonedDateTime.now();
				bufferedWriter.append(code + "\t" + zdt.toString());
    			bufferedWriter.newLine();
        } catch (IOException ioe) {
        	logger.error(ioe.getMessage(), ioe);
        }

      logger.error(e.getMessage(), e);
      //System.exit(STATUS_UPDATE_ERROR);
    }
  }

  private void removeList(Path list) {
    try (BufferedReader reader = Files.newBufferedReader(list)) {
      reader.lines().forEach(this::remove);
    } catch (IOException e) {
      logger.error(e.getMessage(), e);
      // IY add log
      //System.exit(STATUS_UPDATE_ERROR);
    }
  }

  private void remove(String code) {
    try {
      logger.debug(String.format("Remove %s", getOutputPath(OutputType.JSON).resolve(code + ".json.gz")));
      rm(getOutputPath(OutputType.JSON).resolve(code + ".json.gz"));

      logger.debug(String.format("Remove %s", getOutputPath(OutputType.JSONLD).resolve(code + ".jsonld.gz")));
      rm(getOutputPath(OutputType.JSONLD).resolve(code + ".jsonld.gz"));

      logger.debug(String.format("Remove %s", getOutputPath(OutputType.TURTLE).resolve(code + ".ttl.gz")));
      rm(getOutputPath(OutputType.TURTLE).resolve(code + ".ttl.gz"));
    } catch (IOException e) {
        logger.error(e.getMessage(), e);

	    Path path = Paths.get("./RemoveErrorId.txt");
	  	try {
	              FileOutputStream fileOutputStream = new FileOutputStream(path.toFile(), true);
	              OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream,  "UTF-8");
	              BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);

	  			ZonedDateTime zdt = ZonedDateTime.now();
					bufferedWriter.append(code + "\t" + zdt.toString());
	  			bufferedWriter.newLine();
	    } catch (IOException ioe) {
	      	logger.error(ioe.getMessage(), ioe);
	    }

      //System.exit(STATUS_UPDATE_ERROR);
    }
  }
}
