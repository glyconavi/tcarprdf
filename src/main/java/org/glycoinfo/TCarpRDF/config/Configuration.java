package org.glycoinfo.TCarpRDF.config;

import static org.glycoinfo.TCarpRDF.Constants.*;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.nodes.Tag;
import org.yaml.snakeyaml.representer.Representer;

import lombok.Data;

@Data
public class Configuration {

  private static final Logger logger = LoggerFactory.getLogger(Configuration.class);

  public GlyTouCanID glytoucan_id;
  public Repository repository;
  public Converter converter;

  public static Path file() {
    return Paths.get(System.getProperty("user.home"), CONFIG_DIRECTORY_NAME, CONFIG_FILE_NAME);
  }

  public static boolean isExist() {
    return Files.exists(file());
  }

  public static Configuration load() throws IOException {
    return load(file());
  }

  public static Configuration load(Path path) throws IOException {
    Configuration config;

    try {
      config = new Yaml().loadAs(Files.newBufferedReader(path), Configuration.class);
    } catch (NoSuchFileException e) {
      config = new Configuration();
      logger.error("File not found: " + path.toAbsolutePath(), e);
    }

    return config;
  }

  public Configuration() {
    this.glytoucan_id = new GlyTouCanID();
    this.repository = new Repository();
    this.converter = new Converter();
  }

  public String dump() {
    StringWriter out = new StringWriter();

    writeTo(out);

    return out.toString();
  }

  public void save() throws IOException {
    if (!Files.exists(file().getParent())) {
      Files.createDirectory(file().getParent());
    }

    save(Configuration.file());
  }

  public void save(Path path) throws IOException {
    writeTo(Files.newBufferedWriter(path));
  }

  private void writeTo(Writer out) {
    Representer representer = new Representer();
    representer.addClassTag(Configuration.class, Tag.MAP);

    DumperOptions options = new DumperOptions();
    options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);

    new Yaml(representer, options).dump(this, out);
  }
}
