package org.glycoinfo.TCarpRDF.config;

import lombok.Data;

@Data
public class Converter {
  private String input = "";
  private boolean json = false;
  private boolean jsonld = false;
  private boolean turtle = false;
}
