package org.glycoinfo.TCarpRDF.config;

import lombok.Data;

@Data
public class Repository {
  private String path = "";
  private boolean mmjson = false;
  private boolean mmcif = false;
}
