package org.glycoinfo.TCarpRDF.config;

import lombok.Data;

@Data
public class GlyTouCanID {
  private String local = "";
}
