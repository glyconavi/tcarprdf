#!/bin/sh
# workspace = /home/glyconavi/glycosite/virtuoso/data/pdb/tcarp/
echo ls /workspace/
ls /workspace/
echo ls /workspace/tcarprdf/bin
ls /workspace/tcarprdf/bin
echo checking repository
if [ ! -d /workspace/tcarprdf/repository ]; then
  mkdir -p /workspace/tcarprdf/repository;
fi
# to prepare for public files
if [ ! -d /workspace/tcarprdf/repository/out ]; then
  mkdir -p /workspace/tcarprdf/repository/out;
fi
if [ ! -d /workspace/tcarprdf/repository/out/turtle ]; then
  mkdir -p /workspace/tcarprdf/repository/out/turtle;
fi
echo ls /workspace/tcarprdf/repository
ls /workspace/tcarprdf/repository
echo /workspace/tcarprdf/bin/LocalGlyTouCanID > /workspace/tcarprdf/repository/glytoucan.tsv
/workspace/tcarprdf/bin/LocalGlyTouCanID > /workspace/tcarprdf/repository/glytoucan.tsv
