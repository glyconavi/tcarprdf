# glytoucan.org用のTCarpRDFデータ自動同期とロード

## 参考資料

https://gitlab.com/glyconavi/noguchi/documentation/-/blob/master/TCarpRDF.md

https://gitlab.com/glyconavi/noguchi/documentation/-/blob/master/TCarp-Jenkins.md

https://gitlab.com/glyconavi/jenkinsfile/-/blob/master/README.md

## Docker

DockerfileにDockerイメージ内にアプリをコンパイルして実行できるようにする。
GitlabCIのパイプラインに自動的に作成しGitlabのDockerImageリポジトリーにプッシュする。
[実行ログ](https://gitlab.com/glyconavi/tcarprdf/-/pipelines)

## TcarpRDFのconfig設定

configファイルをコミットしてあります。Docker環境内には`root`ユーザのフォルダーに保管してあります。

```
COPY config /root/.repository/config
```

## バッチ用処理（スケジュール時に起動する）

GitlabCIのスケジュール機能により実行を自動的化しています。

詳細は[gitlcab-ci.yml](https://gitlab.com/glyconavi/tcarprdf/-/blob/master/.gitlab-ci.yml)に記載

![image-3.png](./image-3.png)

木曜日に実行されるschedule：
![image-1.png](./image-1.png)

バッチ処理：
![image.png](./image.png)

### gtcupdate

スケジュールは`weekly`時に起動します。

#### フォルダー作成

glyconaviのホームフォルダー`/home/glyconavi`は`docker-compose.folder.yml`では、`/glyconavi/`としてマウントされている。

ホームフォルダーにglyconaviのデータフォルダー`glycosite`が保管されています。`docker-virtuoso`環境のデータフォルダーはデータフォルダーの`virtuoso/data`フォルダー。

#### 起動コマンド

これでvirtuoso環境内からtcarprdfプログラムが生成されるデータにアクセスできるようになります。

```
    - docker-compose -f docker-compose.folder.yml run --rm folder mkdir -p /glyconavi/glycosite/virtuoso/data/pdb/tcarp
    - docker-compose pull
    - docker-compose stop
    - docker-compose rm -f
    - docker-compose up
```

`docker-compose up`を実行する理由：

```
    volumes:
      - /etc/localtime:/etc/localtime
      - tcarprdf-workspace:/workspace
```

`tcarprdf-workspace`のボリュームはDocker内の`/workspace`にマウントされる。
ローカルではマウントされているフォルダーは上記folder.ymlスクリプトが作成されたフォルダー。

```
volumes:
  tcarprdf-workspace:
    driver: local
    driver_opts:
      type: none
      o: bind
      device: /home/glyconavi/glycosite/virtuoso/data/pdb/tcarp/
```

`docker-compose up`を起動とローカルフォルダーが空白になっている場合、Dockerfile内に作成されているフォルダー`/workspace`をコピーする。

Docker内では、`/workspace`フォルダーは最新のソースコードを保存し、コンパイルされているプロジェクトを保管されています。

毎回実行する度、以前のContainer環境を完全に捨てます。`docker-compose rm -rf`

### Docker環境

Dockerfileでは、ソースコードを２回コピーしています。

```
# Copy scripts - mounted onto local folder
COPY . /workspace/tcarprdf
# Copy source for building jar file
COPY . /tcarprdfbuild
```

`/workspace/tcarprdf`は`/home/glyconavi/glycosite/virtuoso/data/pdb/tcarp/tcarprdf`を作成します。Docker内の`root`ユーザとして実行するため、`root`権限になります。

`/tcarprdfbuild`はローカルにマウントされているため、最新のソースコードが入っている、Docker内でビルドされたJava環境。スケジュール化された実行コマンドと、スクリプトの実行フォルダーを編集されたTcarpRDFスクリプトは`bin`フォルダーに保管してある。

### update

`gtcupdate`の完了が成功した場合、`update`が起動します。スケジュールは`weekly`時に起動します。
TcarpRDFのアップデート機能を実行する。

```
  script:
    - docker-compose run --rm tcarprdf /tcarprdfbuild/bin/TCarpRDF update
```

実行される環境設定ファイルは上記にコピーした`repository/config`内に設定されている：

```
glytoucan_id:
  local: '/workspace/tcarprdf/repository/glytoucan.tsv'
repository:
  mmcif: true
  mmjson: false
  path: /workspace/tcarprdf/repository
```

現時点の[設定ファイル](https://gitlab.com/glyconavi/tcarprdf/-/blob/master/config)

TCarpRDFの出力先はvirtuosoとdocker-publicに使わられています。

`workspace`のローカルフォルダーは`/home/glyconavi/glycosite/virtuoso/data/pdb/tcarp/tcarprdf/repository`。詳細は上記の`gtcupdate`を参考にしてください。

tcarpのrepositoryフォルダーを作成します。

テスト環境のリスト：
```
[glyconavi@test ~]$ ls -al /home/glyconavi/glycosite/virtuoso/data/pdb/tcarp/tcarprdf/repository
total 39624
drwxr-xr-x. 5 root root      147 12月 12 19:13 .
drwxr-xr-x. 7 root root      278 12月 12 10:04 ..
-rw-r--r--. 1 root root     1150 12月 19 14:33 added.pdb
-rw-r--r--. 1 root root   924644 12月 19 14:33 all_pdbid.txt.gz
-rw-r--r--. 1 root root 39636023 12月 19 14:32 glytoucan.tsv
drwxr-xr-x. 4 root root       27 12月 13 01:53 mmcif
-rw-r--r--. 1 root root     1105 12月 19 14:33 modified.pdb
-rw-r--r--. 1 root root        5 12月 19 14:33 obsolete.pdb
drwxr-xr-x. 3 root root       20 12月 14 09:31 out
[glyconavi@test ~]$ 
```

`mmcif`と`out`フォルダーなどが作成されます。

```
[glyconavi@test ~]$ ls -al /home/glyconavi/glycosite/virtuoso/data/pdb/tcarp/tcarprdf/repository/out/turtle/
total 2648
drwxr-xr-x. 2 root root  4096 12月 16 09:13 .
drwxr-xr-x. 3 root root    20 12月 14 09:31 ..
-rw-r--r--. 1 root root 22665 12月 16 08:07 6kxs.ttl.gz
-rw-r--r--. 1 root root 14054 12月 16 08:08 6wb4.ttl.gz
-rw-r--r--. 1 root root 14890 12月 16 08:08 6wb5.ttl.gz
-rw-r--r--. 1 root root 27515 12月 16 08:08 6wb7.ttl.gz
-rw-r--r--. 1 root root 75772 12月 16 08:12 6xlu.ttl.gz
-rw-r--r--. 1 root root 64197 12月 16 08:16 6xm0.ttl.gz
-rw-r--r--. 1 root root 65551 12月 16 08:19 6xm3.ttl.gz
```

`out`フォルダーは[docker-public](https://gitlab.com/glyconavi/noguchi/docker-public)の環境内にマウントされて、ダウンロードできるように公開します。

`turtle`は上記の設定ファイルに記載してある形式設定。

### load

`update`の完了が成功した場合、`load`が起動します。スケジュールは`weekly`時に起動します。

docker-virtuoso環境のロードを行います。

```
  script:
    - docker exec -i glycosite-virtuoso /scripts/isql_tcarp.sh
```

```
/usr/local/virtuoso-opensource/bin/isql 1111 $VIRTUOSO_DBA_USER $VIR
TUOSO_DBA_PASS /scripts/load_tcarp.sql -i /virtuoso/pdb/tcarp/tcarprdf/re
pository/out/turtle http://glyconavi.org/tcarp
```

`http://glyconavi.org/tcarp`のグラフにロードします。

### compress

`load`が完了した後、Virtuoso上の圧縮コマンドを実行

```
  script:
    - docker exec -i glycosite-virtuoso /scripts/compress_tcarp.sh
```

`/virtuoso/pdb/tcarp/tcarprdf/repository/out/tcarp.tar.gz`のファイルを作成します。

### copy

`compress`が完了した後、tarファイルを`public`サーバーにコピー。
`/home/glyconavi/glycosite/virtuoso/data/pdb/tcarp/tcarprdf/repository/out/tcarp.tar.gz`

```
    - docker-compose -f docker-compose.folder.yml run --rm folder mkdir -
p /glyconavi/glycosite/public/files/tcarp/TCarpRDF/turtle
    - docker-compose -f docker-compose.folder.yml run --rm folder rsync -
avztPp /glyconavi/glycosite/virtuoso/data/pdb/tcarp/tcarprdf/repository/o
ut/tcarp.tar.gz /glyconavi/glycosite/public/files/tcarp/TCarpRDF/turtle/
```

フォルダー構成を作成して、
`/home/glyconavi/glycosite/public/files/tcarp/TCarpRDF/turtle/tcarp.tar.gz`にファイルをコピーします。

### updateall

TcarpRDFの`update -a`を実行します。手動で行う設定になっています。

```
  script:
    - docker-compose run --rm tcarprdf /tcarprdfbuild/bin/TCarpRDF update -a
 ```

### updateallskipsync

TcarpRDFの`update -a --skip-sync`を実行します。手動で行う設定になっています。

```
  script:
    - docker-compose run --rm tcarprdf /tcarprdfbuild/bin/TCarpRDF update -a --skip-sync
```

### テスト環境のスケジュール

テスト環境用のジョブ設定にはテスト用`weeklytest`スケジュールにより起動します。

## ビルド用ジョブ

下記のファイル・フォルダーが別のブランチに変更された場合、ビルドジョブが起動します：

```
build:
  stage: build
  only:
    refs:
      - branches
    changes:
      - Dockerfile
      - src/**/*
      - run.sh
      - pom.xml
```

![image-2.png](./image-2.png)

手動で行うジョブもビルド用ジョブに表示します。

### 初期実行の場合

初期実行の場合、`/home/glyconavi/glycosite/virtuoso/data/pdb/tcarp/`のフォルダーが空白ではなかった場合*コピーされません*。

コピーするには、新規フォルダーとして作成する必要があるため、既存のフォルダーを別名に移動すれば、コピーされます。

本番環境の例：

```
[glyconavi@sparqlist pdb]$ ls
pdb_20190123.ttl.gz  results  tcarp  wwpdb  wwpdb-cc
[glyconavi@sparqlist pdb]$ mv tcarp tcarp.0

```

### 本番リリース

リリース時、Javaのソースコードの変更の場合、ビルドが発生する。`.env`ファイルに設定している`PROJECT_VERSION`が`docker-compose.yml`にも設定されている。このバージョンによりdocker imageが作成されて、gitlab repositoryにプッシュしている。基本的なビルドとディプロイはテスト環境で実施しているため、`PROJECT_VERSION`と`PROJECT_VERSION_TEST`は同様です。

リリースのジョブ用の本番環境用のバージョン番号は`.env`ファイルに保管していて`PROJECT_VERSION_PROD`のキー名です。リリースジョブは`PROJECT_VERSION_TEST`のバージョンを`PROJECT_VERSION_PROD`にコピーされて、プッシュされます。

### 本番用ジョブ

本番用ジョブでは、`PROJECT_VERSION`を`PROJECT_VERSION_PROD`に設定する必要があるため、docker-compose内の環境変数として`PROJECT_VERSION`を`PROJECT_VERSION_PROD`に設定します。

例：
```
  script:
    - . .env
    - PROJECT_VERSION=$PROJECT_VERSION_PROD docker-compose run --rm tcarprdf /tcarprdfbuild/bin/TCarpRDF update
  tags:
    - prod
```
