# TCarpRDF

## Prerequisites

* Java 1.8+
* Apache Maven 3.5+


## Compile

    mvn clean compile assembly:single


## Usage

### Initialize repository

    $ mkdir repository && cd $_
    $ TCarpRDF init [options]

| Option | Argument | Description |
|---|---|---|
| -f,--force        |                               | Initialize without prompt                                           |
| -h,--help         |                               | Show usage help                                                     |
| -i,--in           | FORMAT=[mmcif\|mmjson]        | Set the input format for converter                                  |
| --local-glytoucan | FILE                          | Set this repository to use local mappings for WURCS to GlyTouCan ID |
| -o,--out          | FORMAT=[json\|jsonld\|turtle] | Set the output format for converter                                 |
| -s,--sync         | FORMAT=[mmcif\|mmjson]        | Set the format to store locally                                     |

#### Note

If you would like to use this program in an environment disconnected from the internet,
generate ID mappings manually by using `bin/LocalGlyTouCanID` and pass the file to argument of `--local-glytoucan`.

    bin/LocalGlyTouCanID > glytoucan.tsv


### Update reopsitory

    $ TCarpRDF update [options]

Synchronize local files with remote, and convert weekly updated entries.
By default, this program processes only entries listed in
- ftp://ftp.pdbj.org/XML/pdbmlplus/latest_new_pdbid.txt
- ftp://ftp.pdbj.org/XML/pdbmlplus/latest_updated_pdbid.txt

If you missed updates of last week, use --all option to keep all converted files up-to-date.

| Option | Description |
|--------|-------------|
| -a, --all      | Convert all entries listed in ftp://ftp.pdbj.org/XML/pdbmlplus/all_pdbid.txt.gz |
| -h, --help     | Show usage help                                                                 |
| --skip-convert | Skip conversion                                      |
| --skip-sync    | Skip directory synchronization with remote                                      |

### Configuration

The config file is expected to be in the home directory:

```
~/.repository/config
```

The following will initialize a config file:

```
TCarpRDF init -i mmcif --local-glytoucan /home/glyconavi/glycosite/virtuoso/data/pdb/tcarp/tcarprdf/repository/glytoucan.tsv -o turtle -s mmcif 
```

The docker environment specific to [glyconavi.org](https://glyconavi.org) is described in [Docker.md](https://gitlab.com/glyconavi/tcarprdf/-/blob/master/Docker.md) (Japanese only).

### ReleaseNote


* 2023/03/16: released ver 1.6.0

    * Changed MolWURCS version to 0.10.0
